// Variables
const socketClient = io();
const txtTituloModulo = document.querySelector('#txtTituloModulo');
const txtTicketAtendido = document.querySelector('#txtTicketAtendido');
const btnTicketSiguiente = document.querySelector('#btnTicketSiguiente');
const divNoHayMasTickets = document.querySelector('#divNoHayMasTickets');
const searchParams = new URLSearchParams(window.location.search);

// MAIN
// If no module variable was sent on Query then throw an error and go back to index.html
if (!searchParams.has('modulo')) {
    window.location = 'index.html';
    throw new Error('Variable escritorio no viene en el URL');
} else {
    const modulo = searchParams.get('modulo');

    txtTituloModulo.innerText = modulo; // Modyfing the title text with the allocated module information

    // When Server is On - then the Next Ticket Button will be Enabled
    socketClient.on('connect', () => {
        btnTicketSiguiente.disabled = false;
        socketClient.emit('tickets-en-cola', {}, (payload) => {
            if (payload.totalTicketsSinAtender <= 0) {
                divNoHayMasTickets.style.display = ''; // We will show message "No Customers in Queue"
                lblPendientes.style.display = 'none' // We wil hide the number of pending customers since its equal to zero
            } else {
                divNoHayMasTickets.style.display = 'none'; // We will hide message "No Customers in Queue"
                lblPendientes.innerText = payload.totalTicketsSinAtender; // Showing how many customers require attention
            }

        })
    });

    // When Server is Off - then the Next Ticket Button will be Disabled
    socketClient.on('disconnect', () => {
        btnTicketSiguiente.disabled = true;
    });

    // Every time we click "Next Customer ..."
    btnTicketSiguiente.addEventListener('click', () => {
        socketClient.emit('atender-ticket', {modulo}, (payload) => {
            if (!payload.ok) {
                txtTicketAtendido.innerText = 'No hay más clientes por atender';
                return divNoHayMasTickets.style.display = ''; // We will show message "No Customers in Queue" when no more tickets
            } else {
                txtTicketAtendido.innerText = payload.ticketPorAtender.numero; // Showing the customer we are attending right now
                if (payload.totalTicketsSinAtender <= 0) {
                    divNoHayMasTickets.style.display = ''; // We will show message "No Customers in Queue"
                    lblPendientes.style.display = 'none' // We wil hide the number of pending customers since its equal to zero
                } else {
                    lblPendientes.innerText = payload.totalTicketsSinAtender; // Showing how many customers require attention
                }
            }
        });
    });

    // Every time a cusotmer is called we need to update all "how many customers require attention" data
    socketClient.on('remaining-tickets', (totalTicketsSinAtender) => {
        if (totalTicketsSinAtender <= 0) {
            divNoHayMasTickets.style.display = ''; // We will show message "No Customers in Queue"
            lblPendientes.style.display = 'none' // We wil hide the number of pending customers since its equal to zero
        } else {
            divNoHayMasTickets.style.display = 'none'; // We will hide message "No Customers in Queue"
            lblPendientes.style.display = '' // We will show the number of pending customers since its equal to zero
            lblPendientes.innerText = totalTicketsSinAtender; // Showing how many customers require attention
        }
    })
}