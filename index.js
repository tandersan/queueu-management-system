// Requires
require('dotenv').config(); // This app will use just .env variables

//Variables
const Server = require('./server/server');

//MAIN
const main = async () => {
    const server = new Server();

    server.subirServidor();
}

// Let's go!
main();