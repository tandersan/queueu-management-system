// Requires
const path = require('path');
const fs = require('fs');

// Variables
const relativeJsonPath = '../database/data.json'; // Our database - JSON FTW!

// MAIN
class Ticket {
    constructor(numero, modulo) {
        this.numero = numero;
        this.modulo = modulo;
    }
}

class TicketControl {
    constructor() {
        this.ultimoTicket = 0; // Last ticket requested by customers
        this.diaActual = new Date().getDate(); // Number of the current day
        this.ticketsPendientes = []; // All pending tickets - let speed up, our customers are waiting
        this.ticketsEnPantalla = []; // Tickets in the public screens

        this.inicializarDB();
    }

    // Returning properties we are going to save in database
    get toJson() {
        return {
            ultimoTicket: this.ultimoTicket,
            diaActual: this.diaActual,
            ticketsPendientes: this.ticketsPendientes,
            ticketsEnPantalla: this.ticketsEnPantalla
        }
    }

    inicializarDB() {
        const dataTickets = require(relativeJsonPath); // Loading database ...

        // If today is same day saved on database we will assume database data is currently being used
        // So any new people entering the app will get last status and continue serving customers
        if (dataTickets.diaActual === this.diaActual) {
            this.ultimoTicket = dataTickets.ultimoTicket;
            this.ticketsPendientes = dataTickets.ticketsPendientes;
            this.ticketsEnPantalla = dataTickets.ticketsEnPantalla;
        // But if database day info is old then we will assume a new day therefor queue system kick off from scratch
        } else {
            this.guardarDB();
        }
    }

    // Saving our database!
    guardarDB() {
        const absoluteJsonPath = path.join(__dirname, relativeJsonPath);
        fs.writeFileSync(absoluteJsonPath, JSON.stringify(this.toJson));
    }

    siguienteTicket() {
        this.ultimoTicket += 1; // We select next ticket
        const ticket = new Ticket(this.ultimoTicket, null); // we create the next ticket structure but without anyone attending it
        this.ticketsPendientes.push(ticket); // Now the ticket is pending
        this.guardarDB();

        return 'Ticket ' + ticket.numero;
    }

    atenderTicket(modulo) {
        // If there is no unattended ticket then we return null
        if (this.ticketsPendientes.length === 0) {
            return null;
        } else {
            const ticketPorAtender = this.ticketsPendientes.shift(); // We get customer next in line and erase it from array
            ticketPorAtender.modulo = modulo; // We assign the module number of the agent that will attend the customer
            this.ticketsEnPantalla.unshift(ticketPorAtender); // Adding the last called customer ticket to the main screen

            // If we have 1 more ticket than the max ammount allowed to show in screen - lets remove the last one
            if (this.ticketsEnPantalla.length > process.env.NUMBERTICKETSCREEN) {
                this.ticketsEnPantalla.splice(-1,1);
            }
            this.guardarDB();

            return ticketPorAtender;
        }
    }

    retornaUltimoTicket() {
        return this.ultimoTicket;
    }

    retornaTicketsEnPatanlla() {
        return this.ticketsEnPantalla;
    }

    retornaTotalTicketsSinAtencion() {
        return this.ticketsPendientes.length;
    }
}

module.exports = TicketControl