// Requires
const TicketControl = require('../models/ticket-control'); // Where the Ticket management magic happens

// Variables
const ticketControl = new TicketControl(); // We get last data in DDBB ; or we reset it. See ticket-control.js for more info

// MAIN
// Reducing server.js complexity by managing our Socket here
const socketController = (socketServer) => {
    socketServer.emit('ultimo-ticket', ticketControl.retornaUltimoTicket());
    socketServer.emit('estado-actual', ticketControl.retornaTicketsEnPatanlla());

    // This when creating new tickets
    socketServer.on('siguiente-ticket', (payload, callback) => {
        const siguienteTicket = ticketControl.siguienteTicket();
        callback(siguienteTicket);
        socketServer.broadcast.emit('remaining-tickets', ticketControl.retornaTotalTicketsSinAtencion());
    })

    // This when attending customers
    socketServer.on('atender-ticket', (payload, callback) => {
        if(!payload.modulo) {
            return callback({
                ok: false,
                msg: 'El módulo es obligatorio'
            });
        } else {
            const ticketPorAtender = ticketControl.atenderTicket(payload.modulo);

            if (!ticketPorAtender) {
                return callback({
                    ok: false,
                    msg: 'No hay clientes por atender'
                });
            } else {
                const retornaTotalTicketsSinAtencion = ticketControl.retornaTotalTicketsSinAtencion();
                socketServer.broadcast.emit('estado-actual', ticketControl.retornaTicketsEnPatanlla());
                socketServer.broadcast.emit('remaining-tickets', retornaTotalTicketsSinAtencion);
                return callback({
                    ok: true,
                    ticketPorAtender,
                    totalTicketsSinAtender: retornaTotalTicketsSinAtencion
                });
            }
        }
    });

    // This to update number of pending customers
    socketServer.on('tickets-en-cola', (payload, callback) => {
        return callback({
            ok: true,
            totalTicketsSinAtender: ticketControl.retornaTotalTicketsSinAtencion()
        });
    });

    // This when we shut down server
    socketServer.on('disconnect', () => {
    })

    // This is User Logged ID
    console.log('Cliente conectado ', socketServer.id);
}

module.exports = {
    socketController
}